![LegoSCRUM](doc/images/LegoSCRUM_title_small.jpg)

**LegoSCRUM** es un proyecto de introducción a **SCRUM** desde un punto de vista 
lúdico, mezclando a partes iguales teoría, practica y... 
**MUCHAS PIEZAS DE LEGO**.

[[_TOC_]]

# Calendario
| Lugar | Fecha | Ponentes | Wiki |
| ----- | ----- | -------- | ---- |
| SCDS | 27/02/2019 18:00-20:00 | Quico, Guillermo & yo | [SCDS](https://gitlab.com/OskarBarrio/sok.learning/wikis/Talleres/LegoSCRUM/TallerSCDS) |
| Universidad de León | 07/03/2019 16:00-18:00 | Quico, Guillermo & yo | TBC |
| SCDS (2) | 26/03/2019 | Quico, Guillermo & yo | TBC |
| Universidad de León (2) | 02/04/2019 17:00-19:30 | Quico, Guillermo & yo | TBC |

# Agenda
Duración: 2.5 horas

- Capítulo 1: CRISIS
  - Introducción a la crisis del software y Agile cómo respuesta
- Capítulo 2: SCRUM
  - SCRUM como arma: Roles, framework, artefactos y Estimación
- Capítulo 3: MARTE
  - El juego, 3 sprints para salvar a la humanidad

# Historia
**La tierra ha colapsado**. El aumento indiscriminado de la población humana 
junto con la ascensión al poder de las máquinas han decidido el destinado de la
humanidad... y hemos perdido.  
Por suerte, un pequeño grupo de colonos han conseguido hacerse en el último 
momento con una nave interplanetaria y han escapado de la aniquilación rumbo a 
Marte. Sin embargo esta nueva aventura no está carente de problemas, ya que 
Marte es un planeta hostil.  
Nuestro capitán Jonathan Oh'Connor ha diseñado un plan para asegurar la 
supervivencia de nuestra especie, pero necesita de tu equipo para construir 
nuestro nuevo hogar.

* [Presentación Prezi](https://prezi.com/p/xqiygjfobpfl/?present=1)
